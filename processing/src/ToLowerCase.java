import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ToLowerCase {
	
    private ServerSocket serverSocket;
    private Socket serverClientSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

	public void setInput(int port) {
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setOutput(int port) {
		try {
			clientSocket = new Socket("localhost", port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void start() {
        try {
            serverClientSocket = serverSocket.accept();
            
        	in = new BufferedReader(new InputStreamReader(serverClientSocket.getInputStream()));
			out = new PrintWriter(clientSocket.getOutputStream(), true);
	        
			String text = in.readLine();
	        String text_out = process(text);
	        out.println(text_out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			in.close();
	        out.close();
	        clientSocket.close();
	        serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String process(String input) {
		return input.toLowerCase();
	}
}
