package lab02;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Hashtable;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Window {

	private JFrame frmPrzetwarzanieTekstu;
	private JButton button;
	private JButton button_1;
	private JButton button_2;
	private JButton button_3;
	private JList<String> list;
	private JList<String> list_1;
	private DefaultListModel<String> model = new DefaultListModel<>();
	private DefaultListModel<String> model_1 = new DefaultListModel<>();
	
	private Hashtable<String, Object> classes_obj = new Hashtable<String, Object>();
	private JTextField textField;
	private JTextField textField_1;
	
	
	private File[] update() {
		File f = new File("D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab02\\app\\bin\\addons");
		File[] files = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith("class");
			}
		});
		return files;
	}
	
	private String processing(String input) {
		try {
			//USTAWIENIE GNIAZDEK
			int port = 2000;
		    ServerSocket serverSocket = new ServerSocket(port);
			
			for (int i = model.getSize() - 1; i >= 0; --i) {
				String element = model.getElementAt(i);
				Object obj = classes_obj.get(element);
				Class<?> cl = obj.getClass();
				Method setOutput = cl.getMethod("setOutput", int.class);
				setOutput.invoke(obj, port);
				++port;
				Method setInput = cl.getMethod("setInput", int.class);
				setInput.invoke(obj, port);
			}
			Socket client = new Socket("localhost", port);
			PrintWriter out = new PrintWriter(client.getOutputStream(), true);
		    
		    //URUCHOMIENIE OBSLUGI GNIAZDEK
		    Socket serverClientSocket = serverSocket.accept();
		    BufferedReader in = new BufferedReader(new InputStreamReader(serverClientSocket.getInputStream()));
			for (int i = model.getSize() - 1; i >= 0; --i) {
				String element = model.getElementAt(i);
				Object obj = classes_obj.get(element);
				Class<?> cl = obj.getClass();
				Method start = cl.getMethod("start", new Class<?>[] {});
				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							start.invoke(obj, new Object[] {});
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				thread.start();
			}
			
		    out.println(input);
		    String text = in.readLine();
			
			serverSocket.close();
			for (int i = model.getSize() - 1; i >= 0; --i) {
				String element = model.getElementAt(i);
				Object obj = classes_obj.get(element);
				Class<?> cl = obj.getClass();
				Method stop = cl.getMethod("stop", new Class<?>[] {});
				stop.invoke(obj, new Object[] {});
			}
			client.close();
			
			return text;
			
		} catch (IOException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return "An error occured while processing";
		}
		
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmPrzetwarzanieTekstu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
		
		File[] files = update();
		for(File file : files) {
			String name = file.getName();
			int len = name.length();
			name = name.substring(0, len - 6); //len(".class") = 6
			model_1.addElement(name);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPrzetwarzanieTekstu = new JFrame();
		frmPrzetwarzanieTekstu.setTitle("Przetwarzanie tekstu");
		frmPrzetwarzanieTekstu.setBounds(100, 100, 691, 619);
		frmPrzetwarzanieTekstu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmPrzetwarzanieTekstu.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Przetwarzanie", null, panel, null);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Tekst wej\u015Bciowy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(108, 70, 468, 133);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 26, 447, 75);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Tekst wyj\u015Bciowy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(106, 315, 468, 133);
		panel.add(panel_2);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(10, 26, 447, 75);
		panel_2.add(textField_1);
		
		JButton btnPrzetwarzaj = new JButton("Przetwarzaj");
		btnPrzetwarzaj.setBounds(252, 238, 179, 41);
		panel.add(btnPrzetwarzaj);
		btnPrzetwarzaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String in = textField.getText();
				String out = processing(in);
				textField_1.setText(out);
			}
		});
		btnPrzetwarzaj.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Ustawienie \u0142a\u0144cucha przetwarzania", null, panel_3, null);
		panel_3.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "U\u017Cywane klasy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(23, 22, 280, 531);
		panel_3.add(panel_4);
		panel_4.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(69, 112, 182, 304);
		panel_4.add(scrollPane);
		
				list = new JList<String>();
				scrollPane.setViewportView(list);
				list.setModel(model);
				list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				list.addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent arg0) {
						int index = list.getSelectedIndex();
						if(index!=-1) {
							if(list.getSelectedIndex() > 0)
								button.setEnabled(true);
							else
								button.setEnabled(false);
							if(list.getSelectedIndex() < (list.getModel().getSize()-1))
								button_1.setEnabled(true);
							else
								button_1.setEnabled(false);
							button_2.setEnabled(true);
						}
						else {
							button.setEnabled(false);
							button_1.setEnabled(false);
							button_2.setEnabled(false);
						}
					}
				});
		
		button = new JButton("\u25B2");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				String element = model.getElementAt(index);
				model.removeElementAt(index);
				model.insertElementAt(element, index-1);
			}
		});
		button.setEnabled(false);
		button.setBounds(9, 222, 54, 23);
		panel_4.add(button);
		
		button_1 = new JButton("\u25BC");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				String element = model.getElementAt(index);
				model.removeElementAt(index);
				model.insertElementAt(element, index+1);
			}
		});
		button_1.setEnabled(false);
		button_1.setBounds(9, 267, 54, 23);
		panel_4.add(button_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(102, 13, 114, 101);
		panel_4.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(Window.class.getResource("/lab02/chart1.png")));
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Window.class.getResource("/lab02/chart2.png")));
		label.setBounds(102, 416, 114, 101);
		panel_4.add(label);
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dost\u0119pne klasy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_5.setBounds(422, 110, 226, 339);
		panel_3.add(panel_5);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(20, 24, 182, 304);
		panel_5.add(scrollPane_1);

		list_1 = new JList<String>();
		list_1.setModel(model_1);
		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list_1.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int index = list_1.getSelectedIndex();
				if(index!=-1)
					button_3.setEnabled(true);
				else button_3.setEnabled(false);
			}
		});
		scrollPane_1.setViewportView(list_1);
		
		button_2 = new JButton("\u25B6");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String element = list.getSelectedValue();
				model.removeElement(element);
				Runtime.getRuntime().gc();
				model_1.addElement(element);
				
				classes_obj.remove(element);
			} 
		});
		button_2.setEnabled(false);
		button_2.setBounds(319, 236, 89, 23);
		panel_3.add(button_2);
		
		button_3 = new JButton("\u25C0");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String element = list_1.getSelectedValue();
				model_1.removeElement(element);
				model.addElement(element);
				
				try {
					URLClassLoader cl = new URLClassLoader(new URL[] {new URL("file:/D:\\\\\\\\Users\\\\\\\\Karol\\\\\\\\Documents\\\\\\\\Uczelnia\\\\\\\\Semestr VI\\\\\\\\JAVA\\\\\\\\lab\\\\\\\\lab02\\\\\\\\app\\\\\\\\bin\\\\\\\\addons/")});
					Class<?> c = cl.loadClass(element);
					Object obj = c.newInstance();
					classes_obj.put(element, obj);
					cl.close();
				} catch (ClassNotFoundException | SecurityException | InstantiationException | IllegalAccessException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button_3.setEnabled(false);
		button_3.setBounds(319, 295, 89, 23);
		panel_3.add(button_3);
	}
}
